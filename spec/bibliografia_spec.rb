require 'spec_helper'
require 'bib'

describe Bibliografia do
    before :each do
        @b1 = Bibliografia.new("autor","titulo", "serie", "editorial", "edicion", "publicacion", "isbn")
    end
    
    it "Debe existir uno o más autores" do
        expect(@b1.autor) == ("autor")
    end
    it "Debe existir un título" do
        expect(@b1.titulo) == ("titulo")
    end
    it "Debe existir o no una serie" do
        expect(@b1.serie) == ("serie")
    end
    it "Debe existir una editorial" do
        expect(@b1.editorial) == ("editorial")
    end
    it "Debe existir un número de edición" do
        expect(@b1.edicion) == ("edicion")
    end
    it "Debe existir una fecha de publicación" do
        expect(@b1.publicacion) == ("publicacion")
    end
    it "Debe existir uno o más números ISBN" do
        expect(@b1.isbn) == ("isbn")
    end
    
end